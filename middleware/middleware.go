package middleware

import (
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/context_value"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/jwt_hash"
)
// sebelum ke delivery masuk ke middleware(gerbang depan)
type HandlerMiddleware struct {
	JwtHash jwt_hash.JwtHash
}

func (h HandlerMiddleware) Auth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Bearer fdskhfdsnfewrtkpotj
		token := c.Request().Header.Get("Authorization")
		if token == "" {
			return next(c)
		}

		tokens := strings.Split(token, " ")
		if len(tokens) != 2 {
			return c.JSON(http.StatusUnauthorized, error_messages.ErrorUnauthorized{Message: "format token tidak sesuai"})
			// return error_messages.ErrorUnauthorized{Message: "format token tidak sesuai"}
		}

		if tokens[0] != "Bearer" {
			return c.JSON(http.StatusUnauthorized, error_messages.ErrorUnauthorized{Message: "tidak ada Bearer"})
		}

		claim, err := h.JwtHash.Decode(tokens[1])
		if err != nil {
			return c.JSON(http.StatusUnauthorized, error_messages.ErrorUnauthorized{Message: "unvalid"})
		}

		ctx := c.Request().Context()
		ctx = context_value.SetEmailOnContext(ctx, claim.Email)
		ctx = context_value.SetRoleOnContext(ctx, claim.Role)
		ctx = context_value.SetUserIDOnContext(ctx, claim.UserID)

		c.SetRequest(c.Request().WithContext(ctx))
		return next(c)
	}
}
