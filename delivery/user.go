package delivery

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/service"
)

type userDelivery struct {
	userService service.UserService
}

// @Summary create user
// @Description create user
// @Tags user
// @Param Authorization header string true "Bearer token"
// @Param user body entity.User true "payload request"
// @Success 201 {object} entity.User
// @Failure 500 {object} string
// @Router /user [post]
func (u userDelivery) create(c echo.Context) error {
	var user entity.User

	// merubah json request ke dalam struct
	err := c.Bind(&user)
	if err != nil {
		err = c.JSON(http.StatusBadRequest, err)

		return err
	}

	user, err = u.userService.Create(c.Request().Context(), user)
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorDuplicate:
			return c.JSON(http.StatusConflict, _err)
		case error_messages.ErrorUnauthorized:
			return c.JSON(http.StatusUnauthorized, _err)
		}

		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusCreated, user)
}

// @Summary read user
// @Description read user
// @Tags user
// @Param Authorization header string true "Bearer token"
// @Param name query string false "name of user"
// @Param role query string false "role of user"
// @Param order query string false "order (asc/desc) of user"
// @Param email query string false "email of user"
// @Param limit query integer false "limit of user"
// @Param page query integer false "page of user"
// @Param status query integer false "status of user"
// @Success 200 {object} entity.UserResponse
// @Failure 500 {object} string
// @Router /user [get]
func (u userDelivery) read(c echo.Context) error {
	ctx := c.Request().Context()
	filter := entity.UserFilter{}

	// param & query param itu berbeda
	// query param itu optional
	// param itu wajib(harus ada)
	filter.Name = c.QueryParam("name")
	filter.Email = c.QueryParam("email")
	filter.Order = c.QueryParam("order")
	filter.Role = c.QueryParam("role")

	limit := c.QueryParam("limit")
	if limit != "" {
		limitInt, err := strconv.Atoi(limit)
		if err != nil {
			return c.JSON(http.StatusBadRequest, err)
		}

		filter.Limit = uint64(limitInt)
	}

	page := c.QueryParam("page")
	if page != "" {
		pageInt, err := strconv.Atoi(page)
		if err != nil {
			return c.JSON(http.StatusBadRequest, err)
		}

		filter.Page = int(pageInt)
	}

	status := c.QueryParam("status")
	if status != "" {
		statuses := strings.Split(status, ",")
		for _, value := range statuses {
			statusInt, err := strconv.Atoi(value)
			if err != nil {
				return c.JSON(http.StatusBadRequest, err)
			}

			filter.Status = append(filter.Status, statusInt)
		}
	}

	users, err := u.userService.Read(ctx, filter)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, users)
}

// @Summary read user by id
// @Description read user by id
// @Tags user
// @Param Authorization header string true "Bearer token"
// @Param id path integer true "id of user"
// @Success 200 {object} entity.UserResponse
// @Failure 500 {object} string
// @Router /user/{id} [get]
func (u userDelivery) readById(c echo.Context) error {
	user := entity.User{}

	ID := c.Param("id")
	if ID != "" {
		IDInt, err := strconv.Atoi(ID)
		if err != nil {
			return c.JSON(http.StatusBadRequest, err)
		}
		user.ID = int64(IDInt)
	}

	users, err := u.userService.ReadById(c.Request().Context(), user.ID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, users)
}

// @Summary delete user
// @Description delete user
// @Tags user
// @Param Authorization header string true "Bearer token"
// @Param id path integer true "id of user"
// @Success 204 {object} entity.User
// @Failure 500 {object} string
// @Router /user/{id} [delete]
func (u userDelivery) delete(c echo.Context) error {
	ctx := c.Request().Context()

	ID := c.Param("id")
	IDInt, err := strconv.Atoi(ID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	err = u.userService.Delete(ctx, int64(IDInt))
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorNotAllowed:
			return c.JSON(http.StatusForbidden, _err)
		}

		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusNoContent, nil)
}

// @Summary update user
// @Description update user
// @Tags user
// @Param Authorization header string true "Bearer token"
// @Param id path integer true "id of user"
// @Param user body entity.User true "payload request"
// @Success 204 {object} entity.User
// @Failure 500 {object} string
// @Router /user/{id} [put]
func (u userDelivery) update(c echo.Context) error {
	var user entity.User

	ID := c.Param("id")
	IDInt, err := strconv.Atoi(ID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	err = c.Bind(&user)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	err = u.userService.Update(c.Request().Context(), int64(IDInt), user)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusNoContent, nil)
}

func RegisterUserRoute(userService service.UserService, e *echo.Echo) {
	handler := userDelivery{
		userService: userService,
	}

	e.POST("/user", handler.create)
	e.GET("/user", handler.read)
	e.GET("/user/:id", handler.readById) // :id = param
	e.PUT("/user/:id", handler.update)
	e.DELETE("/user/:id", handler.delete)
}

// Delete
// 1. ambil id(string) dari param
// 2. ubah id string -> id int
// 3. ubah id int -> id int64
// 4. masukkan ke dalam parameter
