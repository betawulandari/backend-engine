package delivery

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/service"
)

type newsDelivery struct {
	newsService service.NewsService
}

// @Summary create news
// @Description create news
// @Tags news
// @Param Authorization header string true "Bearer token"
// @Param news body entity.News true "payload request"
// @Success 201 {object} entity.News
// @Failure 500 {object} string
// @Router /news [post]
func (n newsDelivery) create(c echo.Context) error {
	var news entity.News

	err := c.Bind(&news)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	news, err = n.newsService.Create(c.Request().Context(), news)
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorUnauthorized:
			return c.JSON(http.StatusUnauthorized, _err)
		}

		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusCreated, news)
}

// @Summary update news
// @Description update news
// @Tags news
// @Param Authorization header string true "Bearer token"
// @Param id path integer true "id of news"
// @Param news body entity.News true "payload request"
// @Success 204 {object} entity.News
// @Failure 500 {object} string
// @Router /news/{id} [put]
func (n newsDelivery) update(c echo.Context) error {
	var news entity.News

	ID := c.Param("id")
	IDInt, err := strconv.Atoi(ID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	err = c.Bind(&news)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	news.ID = int64(IDInt)

	err = n.newsService.Update(c.Request().Context(), int64(IDInt), news)
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorUnauthorized:
			return c.JSON(http.StatusUnauthorized, _err)
		}

		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusNoContent, nil)
}

// @Summary publish news
// @Description publish news
// @Tags news
// @Param Authorization header string true "Bearer token"
// @Param id path integer true "id of news"
// @Success 204 {object} string
// @Failure 500 {object} string
// @Router /news/{id}/publish [patch]
func (n newsDelivery) publish(c echo.Context) error {
	ID := c.Param("id")
	IDInt, err := strconv.Atoi(ID)
	if err != nil {
		return err
	}

	err = n.newsService.Publish(c.Request().Context(), int64(IDInt))
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorUnauthorized:
			return c.JSON(http.StatusUnauthorized, _err)
		}

		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusNoContent, nil)
}

// @Summary unpublish news
// @Description unpublish news
// @Tags news
// @Param Authorization header string true "Bearer token"
// @Param id path integer true "id of news"
// @Success 204 {object} string
// @Failure 500 {object} string
// @Router /news/{id}/unpublish [patch]
func (n newsDelivery) unPublish(c echo.Context) error {
	ID := c.Param("id")
	IDInt, err := strconv.Atoi(ID)
	if err != nil {
		return err
	}

	err = n.newsService.UnPublish(c.Request().Context(), int64(IDInt))
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorUnauthorized:
			return c.JSON(http.StatusUnauthorized, _err)
		}

		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusNoContent, nil)
}

// @Summary read news
// @Description read news
// @Tags news
// @Param title query string false "title of news"
// @Param category_name query string false "category name"
// @Param category_id query integer false "category ID"
// @Param limit query integer false "limit of news"
// @Param page query integer false "page of news"
// @Param status query integer false "status of news"
// @Success 200 {object} entity.NewsResponse
// @Failure 500 {object} string
// @Router /news [get]
func (n newsDelivery) read(c echo.Context) error {
	filter := entity.NewsFilter{}
	filter.Title = c.QueryParam("title")
	filter.CategoryName = c.QueryParam("category_name")

	limit := c.QueryParam("limit")
	if limit != "" {
		limitInt, err := strconv.Atoi(limit)
		if err != nil {
			return c.JSON(http.StatusBadRequest, err)
		}

		filter.Limit = uint64(limitInt)
	}

	page := c.QueryParam("page")
	if page != "" {
		pageInt, err := strconv.Atoi(page)
		if err != nil {
			return c.JSON(http.StatusBadRequest, err)
		}

		filter.Page = int(pageInt)
	}

	status := c.QueryParam("status")
	if status != "" {
		statuses := strings.Split(status, ",")
		for _, value := range statuses {
			statusInt, err := strconv.Atoi(value)
			if err != nil {
				return c.JSON(http.StatusBadRequest, err)
			}

			filter.Status = append(filter.Status, statusInt)
		}
	}

	categoryID := c.QueryParam("category_id")
	if categoryID != "" {
		categoryIDInt, err := strconv.Atoi(categoryID)
		if err != nil {
			return c.JSON(http.StatusBadRequest, err)
		}

		filter.CategoryID = categoryIDInt
	}

	newses, err := n.newsService.Read(c.Request().Context(), filter)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, newses)
}

// @Summary read news by id
// @Description read news by id
// @Tags news
// @Param id path integer true "id of news"
// @Success 200 {object} entity.NewsResponse
// @Failure 500 {object} string
// @Router /news/{id} [get]
func (n newsDelivery) readById(c echo.Context) error {
	news := entity.News{}

	ID := c.Param("id")
	if ID != "" {
		IDInt, err := strconv.Atoi(ID)
		if err != nil {
			return c.JSON(http.StatusBadRequest, err)
		}
		news.ID = int64(IDInt)
	}

	newses, err := n.newsService.ReadById(c.Request().Context(), news.ID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, newses)
}

func RegisterNewsRoute(newsService service.NewsService, e *echo.Echo) {
	handler := newsDelivery{
		newsService: newsService,
	}

	e.POST("/news", handler.create)
	e.PUT("/news/:id", handler.update)
	e.PATCH("/news/:id/publish", handler.publish)
	e.PATCH("/news/:id/unpublish", handler.unPublish)
	e.GET("/news", handler.read)
	e.GET("/news/:id", handler.readById)
}
