package delivery

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/service"
)

type categoryDelivery struct {
	categoryService service.CategoryService
}

// @Summary create category
// @Description create category
// @Tags category
// @Param Authorization header string true "Bearer token"
// @Param category body entity.Category true "payload request"
// @Success 201 {object} entity.Category
// @Failure 500 {object} string
// @Router /category [post]
func (c categoryDelivery) create(e echo.Context) error {
	var category entity.Category

	err := e.Bind(&category)
	if err != nil {
		return e.JSON(http.StatusBadRequest, err)
	}

	// salah satu fungsi ctx = dititipi nilai
	category, err = c.categoryService.Create(e.Request().Context(), category)
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorUnauthorized:
			return e.JSON(http.StatusUnauthorized, _err)
		}

		return e.JSON(http.StatusInternalServerError, err)
	}

	return e.JSON(http.StatusCreated, category)
}

// @Summary read category
// @Description read category
// @Tags category
// @Param name query string false "name of category"
// @Param status query string false "status of category"
// @Param order query string false "order (asc/desc) of category"
// @Success 200 {object} entity.CategoryResponse
// @Failure 500 {object} string
// @Router /category [get]
func (c categoryDelivery) read(e echo.Context) error {
	ctx := e.Request().Context()
	filter := entity.CategoryFilter{}

	filter.Name = e.QueryParam("name")
	filter.Order = e.QueryParam("order")

	status := e.QueryParam("status")
	if status != "" {
		statuses := strings.Split(status, ",")
		for _, value := range statuses {
			StatusInt, err := strconv.Atoi(value)
			if err != nil {
				return e.JSON(http.StatusBadRequest, err)
			}

			filter.Status = append(filter.Status, StatusInt)
		}
	}

	categoryResponse, err := c.categoryService.Read(ctx, filter)
	if err != nil {
		return e.JSON(http.StatusInternalServerError, err)
	}

	return e.JSON(http.StatusOK, categoryResponse)

}

// @Summary update category
// @Description update category
// @Tags category
// @Param Authorization header string true "Bearer token"
// @Param id path integer true "id of category"
// @Param category body entity.Category true "payload request"
// @Success 204 {object} entity.Category
// @Failure 500 {object} string
// @Router /category/{id} [put]
func (c categoryDelivery) update(e echo.Context) error {
	var category entity.Category

	id := e.Param("id")
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return e.JSON(http.StatusBadRequest, err)
	}

	err = e.Bind(&category)
	if err != nil {
		return e.JSON(http.StatusInternalServerError, err)
	}

	err = c.categoryService.Update(e.Request().Context(), int64(idInt), category)
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorUnauthorized:
			return e.JSON(http.StatusUnauthorized, _err)
		}

		return e.JSON(http.StatusInternalServerError, err)
	}

	return e.JSON(http.StatusNoContent, nil)
}

// @Summary delete category
// @Description delete category
// @Tags category
// @Param Authorization header string true "Bearer token"
// @Param id path integer true "id of category"
// @Success 204 {object} entity.Category
// @Failure 500 {object} string
// @Router /category/{id} [delete]
func (c categoryDelivery) delete(e echo.Context) error {

	id := e.Param("id")
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return e.JSON(http.StatusBadRequest, err)
	}

	err = c.categoryService.Delete(e.Request().Context(), int64(idInt))
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorUnauthorized:
			return e.JSON(http.StatusUnauthorized, _err)
		}

		return e.JSON(http.StatusInternalServerError, err)
	}

	return e.JSON(http.StatusNoContent, nil)
}

func RegisterCategoryRoute(categoryService service.CategoryService, e *echo.Echo) {
	handler := categoryDelivery{
		categoryService: categoryService,
	}

	e.POST("/category", handler.create)
	e.GET("/category", handler.read)
	e.PUT("/category/:id", handler.update)
	e.DELETE("/category/:id", handler.delete)
}
