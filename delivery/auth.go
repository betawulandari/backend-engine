package delivery

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/service"
)

type authDelivery struct {
	authService service.AuthService
}

type credential struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type successLogin struct {
	Token string `json:"token" example:"pqowieurytlaksjdhfgmnbvzxcghdeiytcbgdhdk"`
}

// param namanya dimana type-data required desc

// @Summary login
// @Description login
// @Tags login
// @Param payload body credential true "email and password"
// @Success 200 {object} successLogin
// @Failure 500 {object} string
// @Router /login [post]
func (a authDelivery) login(c echo.Context) error {
	cred := credential{}

	err := c.Bind(&cred)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	token, err := a.authService.Login(c.Request().Context(), cred.Email, cred.Password)
	if err != nil {
		switch _err := err.(type) {
		case error_messages.ErrorUnauthorized:
			return c.JSON(http.StatusUnauthorized, _err)

		case error_messages.ErrorNotFound:
			return c.JSON(http.StatusNotFound, _err)
		}

		return c.JSON(http.StatusInternalServerError, err)
	}
	
	return c.JSON(http.StatusOK, successLogin{Token: token})
}

func RegisterAuthRoute(authService service.AuthService, e *echo.Echo) {
	handler := authDelivery{
		authService: authService,
	}

	e.POST("/login", handler.login)
}
