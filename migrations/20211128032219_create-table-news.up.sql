CREATE table news (
    id int not null auto_increment,
    title varchar(100) not null,
    content text not null,
    thumbnail varchar(255),
    image varchar(255),
    user_id int not null,
    status tinyint(1) not null,
    created_at TIMESTAMP(3) default CURRENT_TIMESTAMP(3),
    updated_at timestamp(3) default null,
    created_by varchar(100) not null,
    updated_by varchar(100) default null,
    primary key(id)
);