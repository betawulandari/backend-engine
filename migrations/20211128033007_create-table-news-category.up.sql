create table news_category (
    id int not null auto_increment,
    news_id int not null,
    category_id int not null,
    created_at TIMESTAMP(3) default CURRENT_TIMESTAMP(3),
    created_by varchar(100) not null,
    primary key(id)
);