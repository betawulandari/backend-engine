CREATE TABLE user (
  id int not null auto_increment, 
  name varchar(100) not null, 
  email varchar(100) not null, 
  role varchar(100) not null, 
  status tinyint(1) not null, 
  created_at TIMESTAMP(3) default CURRENT_TIMESTAMP(3), 
  updated_at timestamp(3) default null, 
  created_by varchar(100) not null, 
  updated_by varchar(100) default null, 
  primary key(id)
);
