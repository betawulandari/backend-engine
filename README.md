# backend-engine

skripsi beta wulandari

## migrations 
- berisi file untuk membuat tabel
- cara membuat 
    - migrate create -ext sql -dir migrations nama-migrations

## mocking
- cd ke folder yang ada interface
- cara membuat
    - mockery --name=name-interface

## how to run this project
- clone branch develop
- execute file migrations (yang up saja)
- fill value in file .env (.env.example)
- open terminal then excute command "go mod vendor"
- run project with command "go run main.go"
- open browser, copas http://localhost:8080/swagger/index.html

- run unit test with command
    - open terminal
    - then execute command "go test ./... -race -cover" OR open file category_test then click run test

