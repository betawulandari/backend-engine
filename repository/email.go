package repository

import (
	"fmt"
	"net/smtp"
)

type emailRepo struct {
	Host     string
	Port     int
	Sender   string
	Email    string
	Password string
}

func (e emailRepo) Send(email string, password string) error {
	body := "From: " + e.Sender + "\n" +
		"To: " + email + "\n" +
		"Subject: Admin Kabar Informatika\n\n" +
		"Silahkan mengakses API Kabar Informatika dengan menggunakan password ini: " + password

	auth := smtp.PlainAuth("", e.Email, e.Password, e.Host)
	smtpAddr := fmt.Sprintf("%s:%d", e.Host, e.Port)

	err := smtp.SendMail(smtpAddr, auth, e.Email, []string{email}, []byte(body))
	if err != nil {
		return err
	}

	return nil
}

func NewEmailRepository(host string, port int, sender string, email string, password string) EmailRepository {
	return emailRepo{
		Host:     host,
		Port:     port,
		Sender:   sender,
		Email:    email,
		Password: password,
	}
}
