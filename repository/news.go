package repository

import (
	"context"
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
)

type newsRepo struct {
	db *sql.DB
}

// Create news
func (n newsRepo) Create(ctx context.Context, news entity.News) (entity.News, error) {
	tx, err := n.db.Begin()
	if err != nil {
		return entity.News{}, err
	}

	timeNow := time.Now()
	news.CreatedAt = timeNow
	news.Status = 0

	query, args, err := sq.Insert("news").
		Columns("title",
			"content",
			"thumbnail",
			"image",
			"user_id",
			"status",
			"created_at",
			"created_by").
		Values(news.Title,
			news.Content,
			news.Thumbnail,
			news.Image,
			news.UserID,
			news.Status,
			news.CreatedAt,
			news.CreatedBy).
		ToSql()
	if err != nil {
		return entity.News{}, err
	}

	var errRollback error // error undo
	res, err := tx.ExecContext(ctx, query, args...)
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error:", errRollback)
		}

		return entity.News{}, err
	}

	news.ID, err = res.LastInsertId()
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error:", errRollback)
		}

		return entity.News{}, err
	}

	// INSERT INTO MyTable ( Column1, Column2 ) VALUES
	// ( Value1, Value2 ),
	// ( Value1, Value2 )

	qInsert := sq.Insert("news_category").
		Columns("news_id",
			"category_id",
			"created_at",
			"created_by")

	for _, categoryID := range news.CategoryIDs {
		qInsert = qInsert.Values(news.ID,
			categoryID,
			timeNow,
			news.CreatedBy)
	}

	query, args, err = qInsert.ToSql()
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error", errRollback)
		}

		return entity.News{}, err
	}

	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error", errRollback)
		}

		return entity.News{}, err
	}

	err = tx.Commit()
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error:", errRollback)
		}

		return entity.News{}, err
	}

	return news, nil
}

func (n newsRepo) Update(ctx context.Context, ID int64, news entity.News) error {
	tx, err := n.db.Begin()
	if err != nil {
		return err
	}

	var (
		errRollback error
		timeNow     = time.Now()
	)

	news.CreatedAt = timeNow

	query, args, err := sq.Update("news").
		Set("title", news.Title).
		Set("content", news.Content).
		Set("thumbnail", news.Thumbnail).
		Set("image", news.Image).
		Set("updated_at", timeNow).
		Set("updated_by", news.UpdatedBy).
		Where(sq.Eq{"id": ID}).
		ToSql()
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error:", errRollback)
		}

		return err
	}

	query, args, err = sq.Delete("news_category").
		Where(sq.Eq{"news_id": ID}).
		ToSql()
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error:", errRollback)
		}

		return err
	}

	qInsert := sq.Insert("news_category").
		Columns("news_id",
			"category_id",
			"created_at",
			"created_by")

	for _, categoryID := range news.CategoryIDs {
		qInsert = qInsert.Values(ID,
			categoryID,
			timeNow,
			news.CreatedBy)
	}

	query, args, err = qInsert.ToSql()
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error:", errRollback)
		}

		return err
	}

	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error:", errRollback)
		}

		return err
	}

	err = tx.Commit()
	if err != nil {
		errRollback = tx.Rollback()
		if errRollback != nil {
			fmt.Println("error:", errRollback)
		}

		return err
	}

	return nil
}

func (n newsRepo) Publish(ctx context.Context, ID int64, email string) error {

	query, args, err := sq.Update("news").
		Set("status", 1).
		Set("updated_at", time.Now()).
		Set("updated_by", email).
		Where(sq.Eq{"id": ID}).
		ToSql()
	if err != nil {
		return err
	}

	_, err = n.db.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}

func (n newsRepo) UnPublish(ctx context.Context, ID int64, email string) error {

	query, args, err := sq.Update("news").
		Set("status", 0).
		Set("updated_at", time.Now()).
		Set("updated_by", email).
		Where(sq.Eq{"id": ID}).
		ToSql()
	if err != nil {
		return err
	}

	_, err = n.db.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}

func (n newsRepo) Read(ctx context.Context, filter entity.NewsFilter) ([]entity.News, error) {
	order := "n.created_at desc"
	if filter.Order == "asc" {
		order = "n.created_at asc"
	}

	qSelect := sq.Select("n.id",
		"n.title",
		"n.content",
		"n.thumbnail",
		"n.image",
		"n.status",
		"n.user_id",
		"GROUP_CONCAT(c.name) category_name",
		"GROUP_CONCAT(nc.category_id) category_id",
		"GROUP_CONCAT(c.status) category_status").
		From("news n").
		InnerJoin("news_category nc ON n.id = nc.news_id").
		InnerJoin("category c ON c.id = nc.category_id").
		OrderBy(order).
		GroupBy("n.id")
	if filter.Limit != 0 {
		qSelect = qSelect.Limit(filter.Limit)
	}

	if filter.Title != "" {
		qSelect = qSelect.Where(sq.Eq{"n.title": filter.Title})
	}

	if filter.Page != 0 {
		qSelect = qSelect.Offset((uint64(filter.Page) - 1) * filter.Limit)
	}

	if len(filter.Status) > 0 {
		qSelect = qSelect.Where(sq.Eq{"n.status": filter.Status})
	}

	if filter.CategoryName != "" {
		qSelect = qSelect.Where(sq.Eq{"c.name": filter.CategoryName})
	}

	if filter.CategoryID != 0 {
		qSelect = qSelect.Where(sq.Eq{"nc.category_id": filter.CategoryID})
	}

	query, args, err := qSelect.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := n.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	newses := make([]entity.News, 0)
	for rows.Next() {
		var (
			news           entity.News
			categoryID     string
			categoryName   string
			categoryStatus string
		)

		// scan = merubah sql -> variabel
		err = rows.Scan(
			&news.ID,
			&news.Title,
			&news.Content,
			&news.Thumbnail,
			&news.Image,
			&news.Status,
			&news.UserID,
			&categoryName,
			&categoryID,
			&categoryStatus)
		if err != nil {
			return nil, err
		}

		categoryIDs := strings.Split(categoryID, ",")
		categoryNames := strings.Split(categoryName, ",")
		categoryStatuses := strings.Split(categoryStatus, ",")

		for i, cName := range categoryNames {
			cID := categoryIDs[i]
			cIDInt, err := strconv.Atoi(cID)
			if err != nil {
				return nil, err
			}

			cStatus := categoryStatuses[i]
			cStatusInt, err := strconv.Atoi(cStatus)
			if err != nil {
				return nil, err
			}

			category := entity.Category{
				ID:     int64(cIDInt),
				Name:   cName,
				Status: cStatusInt,
			}

			news.Categories = append(news.Categories, category)
			news.CategoryIDs = append(news.CategoryIDs, cIDInt)
		}

		newses = append(newses, news)
	}

	return newses, nil
}

func (n newsRepo) Count(ctx context.Context, filter entity.NewsFilter) (int, error) {
	qCount := sq.Select("COUNT(DISTINCT(n.id))").
		From("news n").
		InnerJoin("news_category nc ON n.id = nc.news_id").
		InnerJoin("category c ON c.id = nc.category_id")

	if len(filter.Status) > 0 {
		qCount = qCount.Where(sq.Eq{"n.status": filter.Status})
	}

	if filter.CategoryID != 0 {
		qCount = qCount.Where(sq.Eq{"c.id": filter.CategoryID})
	}

	if filter.CategoryName != "" {
		qCount = qCount.Where(sq.Eq{"c.name": filter.CategoryName})
	}

	query, args, err := qCount.ToSql()
	if err != nil {
		return 0, err
	}

	// hasil dari rows dimasukkan ke count menggunakan scan
	row := n.db.QueryRowContext(ctx, query, args...)
	var count int
	err = row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (n newsRepo) ReadById(ctx context.Context, ID int64) (entity.News, error) {
	query, args, err := sq.Select("n.id",
		"n.title",
		"n.content",
		"n.thumbnail",
		"n.image",
		"n.status",
		"n.user_id",
		"GROUP_CONCAT(c.name) category_name",
		"GROUP_CONCAT(nc.category_id) category_id",
		"GROUP_CONCAT(c.status) category_status").
		From("news n").
		InnerJoin("news_category nc ON n.id = nc.news_id").
		InnerJoin("category c ON c.id = nc.category_id").
		Where(sq.Eq{"n.id": ID}).
		GroupBy("n.id").
		ToSql()
	if err != nil {
		return entity.News{}, err
	}

	row := n.db.QueryRowContext(ctx, query, args...)
	var (
		news           entity.News
		categoryID     string
		categoryName   string
		categoryStatus string
	)

	// scan = merubah sql -> variabel
	err = row.Scan(
		&news.ID,
		&news.Title,
		&news.Content,
		&news.Thumbnail,
		&news.Image,
		&news.Status,
		&news.UserID,
		&categoryName,
		&categoryID,
		&categoryStatus)
	if err != nil {
		return entity.News{}, err
	}

	categoryIDs := strings.Split(categoryID, ",")
	categoryNames := strings.Split(categoryName, ",")
	categoryStatuses := strings.Split(categoryStatus, ",")

	for i, cName := range categoryNames {
		cID := categoryIDs[i]
		cIDInt, err := strconv.Atoi(cID)
		if err != nil {
			return entity.News{}, err
		}

		cStatus := categoryStatuses[i]
		cStatusInt, err := strconv.Atoi(cStatus)
		if err != nil {
			return entity.News{}, err
		}

		category := entity.Category{
			ID:     int64(cIDInt),
			Name:   cName,
			Status: cStatusInt,
		}

		news.Categories = append(news.Categories, category)
		news.CategoryIDs = append(news.CategoryIDs, cIDInt)
	}

	return news, nil
}

func NewNewsRepository(db *sql.DB) NewsRepository {
	return newsRepo{
		db: db,
	}
}
