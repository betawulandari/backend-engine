package repository

import (
	"context"
	"database/sql"

	sq "github.com/Masterminds/squirrel"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
)

type authRepo struct {
	db *sql.DB
}

func (a authRepo) Login(ctx context.Context, email string) (entity.User, error) {
	query, args, err := sq.Select("id",
		"name",
		"email",
		"role",
		"password",
		"status").
		From("user").
		Where(sq.Eq{"email": email}).
		ToSql()
	if err != nil {
		return entity.User{}, err
	}

	row := a.db.QueryRowContext(ctx, query, args...)
	var user entity.User
	err = row.Scan(&user.ID,
		&user.Name,
		&user.Email,
		&user.Role,
		&user.Password,
		&user.Status)
	if err != nil {
		// ErrNoRows = error, tidak ada baris
		if err == sql.ErrNoRows {
			return entity.User{}, error_messages.ErrorNotFound{Message: "email not found"}
		}

		return entity.User{}, err
	}

	return user, nil
}

func NewAuthRepository(db *sql.DB) AuthRepository {
	return authRepo{
		db: db,
	}
}
