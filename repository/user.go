package repository

import (
	"context"
	"database/sql"
	"time"

	sq "github.com/Masterminds/squirrel"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
)

type userRepo struct {
	db *sql.DB
}

// Create ...
func (u userRepo) Create(ctx context.Context, user entity.User) (entity.User, error) {
	user.CreatedAt = time.Now()
	user.Status = 1
	user.Role = "admin"

	query, args, err := sq.Insert("user").
		Columns("name",
			"email",
			"role",
			"password",
			"status",
			"created_at",
			"created_by").
		Values(user.Name,
			user.Email,
			user.Role,
			user.Password,
			user.Status,
			user.CreatedAt,
			user.CreatedBy).
		ToSql()
	if err != nil {
		return entity.User{}, err
	}

	res, err := u.db.ExecContext(ctx, query, args...)
	if err != nil {
		return entity.User{}, err
	}

	user.ID, err = res.LastInsertId()
	if err != nil {
		return entity.User{}, err
	}

	return user, nil
}

// Read ...
func (u userRepo) Read(ctx context.Context, filter entity.UserFilter) ([]entity.User, error) {
	order := "created_at desc"
	if filter.Order == "asc" {
		order = "created_at asc"
	}

	qSelect := sq.Select("id",
		"name",
		"email",
		"role",
		"status").
		From("user").
		OrderBy(order)
	if filter.Limit != 0 {
		qSelect = qSelect.Limit(filter.Limit)
	}

	if filter.Email != "" {
		qSelect = qSelect.Where(sq.Eq{"email": filter.Email})
	}

	if filter.Name != "" {
		qSelect = qSelect.Where(sq.Eq{"name": filter.Name})
	}

	if filter.Page != 0 {
		qSelect = qSelect.Offset((uint64(filter.Page) - 1) * filter.Limit)
	}

	if filter.Role != "" {
		qSelect = qSelect.Where(sq.Eq{"role": filter.Role})
	}

	if len(filter.Status) > 0 {
		qSelect = qSelect.Where(sq.Eq{"status": filter.Status})
	}

	query, args, err := qSelect.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := u.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	defer rows.Close() // menutup koneksi sql

	// null
	// []
	users := make([]entity.User, 0)
	for rows.Next() {
		var user entity.User

		err = rows.Scan(
			&user.ID,
			&user.Name,
			&user.Email,
			&user.Role,
			&user.Status)
		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	return users, nil
}

// Read (GET) by id
func (u userRepo) ReadById(ctx context.Context, ID int64) (entity.User, error) {
	query, args, err := sq.Select("id",
		"name",
		"email",
		"role",
		"status").
		From("user").
		Where(sq.Eq{"id": ID}).
		ToSql()
	if err != nil {
		return entity.User{}, err
	}

	row := u.db.QueryRowContext(ctx, query, args...)
	var user entity.User
	err = row.Scan(&user.ID,
		&user.Name,
		&user.Email,
		&user.Role,
		&user.Status)
	if err != nil {
		return entity.User{}, err
	}

	return user, nil
}

// Delete ...
func (u userRepo) Delete(ctx context.Context, ID int64) error {
	query, args, err := sq.Update("user").
		Set("status", 0).
		Set("updated_at", time.Now()).
		Where(sq.Eq{"id": ID}).
		ToSql()
	if err != nil {
		return err
	}

	_, err = u.db.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}

// Update
func (u userRepo) Update(ctx context.Context, ID int64, user entity.User) error {
	query, args, err := sq.Update("user").
		Set("name", user.Name).
		Set("status", user.Status).
		Set("updated_at", time.Now()).
		Set("updated_by", user.UpdatedBy).
		Where(sq.Eq{"id": ID}).
		ToSql()
	if err != nil {
		return err
	}

	_, err = u.db.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}

// Count
func (u userRepo) Count(ctx context.Context, filter entity.UserFilter) (int, error) {
	qCount := sq.Select("COUNT(id)").
		From("user")

	if filter.Email != "" {
		qCount = qCount.Where(sq.Eq{"email": filter.Email})
	}

	if filter.Name != "" {
		qCount = qCount.Where(sq.Eq{"name": filter.Name})
	}

	if filter.Role != "" {
		qCount = qCount.Where(sq.Eq{"role": filter.Role})
	}

	if len(filter.Status) > 0 {
		qCount = qCount.Where(sq.Eq{"status": filter.Status})
	}

	query, args, err := qCount.ToSql()
	if err != nil {
		return 0, err
	}

	// hasil dari row dimasukkan ke count menggunakan scan
	row := u.db.QueryRowContext(ctx, query, args...)
	var count int
	err = row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func NewUserRepository(db *sql.DB) UserRepository {
	return userRepo{
		db: db,
	}
}
