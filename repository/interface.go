package repository

import (
	"context"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
)

type UserRepository interface {
	// func(args...) output
	Create(ctx context.Context, user entity.User) (entity.User, error)
	Read(ctx context.Context, filter entity.UserFilter) ([]entity.User, error)
	ReadById(ctx context.Context, ID int64) (entity.User, error)
	Update(ctx context.Context, ID int64, user entity.User) error
	Delete(ctx context.Context, ID int64) error
	Count(ctx context.Context, filter entity.UserFilter) (int, error)
}

type AuthRepository interface {
	Login(ctx context.Context, email string) (entity.User, error)
}

type NewsRepository interface {
	Create(ctx context.Context, news entity.News) (entity.News, error)
	Update(ctx context.Context, ID int64, news entity.News) error
	Publish(ctx context.Context, ID int64, email string) error
	UnPublish(ctx context.Context, ID int64, email string) error
	Read(ctx context.Context, filter entity.NewsFilter) ([]entity.News, error)
	Count(ctx context.Context, filter entity.NewsFilter) (int, error)
	ReadById(ctx context.Context, ID int64) (entity.News, error)
}

type CategoryRepository interface {
	Create(ctx context.Context, category entity.Category) (entity.Category, error)
	Read(ctx context.Context, filtering entity.CategoryFilter) ([]entity.Category, error)
	Count(ctx context.Context, filter entity.CategoryFilter) (int, error)
	Update(ctx context.Context, ID int64, category entity.Category) error
	Delete(ctx context.Context, ID int64) error
}

type EmailRepository interface {
	Send(email string, password string) error
}