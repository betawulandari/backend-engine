package repository

import (
	"context"
	"database/sql"
	"time"

	sq "github.com/Masterminds/squirrel"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
)

type categoryRepo struct {
	db *sql.DB
}

func (c categoryRepo) Create(ctx context.Context, category entity.Category) (entity.Category, error) {
	category.CreatedAt = time.Now()
	category.Status = 1

	query, args, err := sq.Insert("category").
		Columns("name",
			"status",
			"created_at",
			"created_by").
		Values(category.Name,
			category.Status,
			category.CreatedAt,
			category.CreatedBy).
		ToSql()
	if err != nil {
		return entity.Category{}, err
	}

	res, err := c.db.ExecContext(ctx, query, args...)
	if err != nil {
		return entity.Category{}, err
	}

	category.ID, err = res.LastInsertId()
	if err != nil {
		return entity.Category{}, err
	}

	return category, nil
}

func (c categoryRepo) Read(ctx context.Context, filter entity.CategoryFilter) ([]entity.Category, error) {
	order := "created_at desc"
	if filter.Order == "asc" {
		order = "created_at asc"
	}

	qSelect := sq.Select("id",
		"name",
		"status").
		From("category").
		OrderBy(order)

	if filter.Name != "" {
		qSelect = qSelect.Where(sq.Eq{"name": filter.Name})
	}

	if len(filter.Status) > 0 {
		qSelect = qSelect.Where(sq.Eq{"status": filter.Status})
	}

	query, args, err := qSelect.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := c.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	categories := make([]entity.Category, 0)
	for rows.Next() {
		var category entity.Category

		err = rows.Scan(
			&category.ID,
			&category.Name,
			&category.Status)
		if err != nil {
			return nil, err
		}

		categories = append(categories, category)
	}

	return categories, nil
}

func (c categoryRepo) Count(ctx context.Context, filter entity.CategoryFilter) (int, error) {
	qCount := sq.Select("COUNT(id)").
		From("category")

	if filter.Name != "" {
		qCount = qCount.Where(sq.Eq{"name": filter.Name})
	}

	if len(filter.Status) > 0 {
		qCount = qCount.Where(sq.Eq{"status": filter.Status})
	}

	query, args, err := qCount.ToSql()
	if err != nil {
		return 0, err
	}

	// hasil dari row dimasukkan ke count menggunakan scan
	row := c.db.QueryRowContext(ctx, query, args...)
	var count int
	err = row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (c categoryRepo) Update(ctx context.Context, ID int64, category entity.Category) error {
	query, args, err := sq.Update("category").
		Set("name", category.Name).
		Set("updated_at", time.Now()).
		Set("updated_by", category.UpdatedBy).
		Where(sq.Eq{"id": ID}).
		ToSql()
	if err != nil {
		return err
	}

	_, err = c.db.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}

func (c categoryRepo) Delete(ctx context.Context, ID int64) error {
	var category entity.Category

	query, args, err := sq.Update("category").
		Set("status", 0).
		Set("updated_at", time.Now()).
		Set("updated_by", category.UpdatedBy).
		Where(sq.Eq{"id": ID}).
		ToSql()
	if err != nil {
		return err
	}

	_, err = c.db.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}

func NewCategoryRepository(db *sql.DB) CategoryRepository {
	return categoryRepo{
		db: db,
	}
}
