package error_messages

type ErrorNotAllowed struct {
	Message string
}

func (e ErrorNotAllowed) Error() string {
	return e.Message
}

type ErrorDuplicate struct {
	Message string
}

func (e ErrorDuplicate) Error() string {
	return e.Message
}

type ErrorUnauthorized struct {
	Message string
}

func (e ErrorUnauthorized) Error() string {
	return e.Message
}

type ErrorNotFound struct {
	Message string
}

func (e ErrorNotFound) Error() string {
	return e.Message
}