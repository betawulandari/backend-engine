package jwt_hash

import (
	"github.com/golang-jwt/jwt"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
)

type jwtHash struct {
	SecretKey []byte
}

func (j jwtHash) Encode(c Claim) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &c)
	tokenString, err := token.SignedString(j.SecretKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (j jwtHash) Decode(tokenString string) (Claim, error) {
	c := Claim{}

	token, err := jwt.ParseWithClaims(tokenString, &c, func(t *jwt.Token) (interface{}, error) {
		return j.SecretKey, nil
	})
	if err != nil {
		return Claim{}, err
	}

	if !token.Valid {
		return Claim{}, error_messages.ErrorUnauthorized{Message: "token unvalid"}
	}

	return c, nil

}

func NewJwtHash(secretKey []byte) JwtHash {
	return jwtHash{
		SecretKey: secretKey,
	}
}
