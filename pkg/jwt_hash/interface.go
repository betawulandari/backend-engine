package jwt_hash

import (
	"github.com/golang-jwt/jwt"
)

type Claim struct {
	Email  string
	Role   string
	UserID int64
	jwt.StandardClaims
}

type JwtHash interface {
	Encode(c Claim) (string, error)     // dari informasi berubah jadi token (di enkripsi)
	Decode(token string) (Claim, error) // kebalikan dari encode (dari token berubah jadi informasi)
}
