package context_value

import (
	"context"

	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
)

type ctxKey string

var (
	keyEmail ctxKey = "email"
	keyRole  ctxKey = "role"
	keyUserID ctxKey = "user_id"
)

func SetEmailOnContext(ctx context.Context, email string) context.Context {
	ctx = context.WithValue(ctx, keyEmail, email)

	return ctx
}

func SetRoleOnContext(ctx context.Context, role string) context.Context {
	ctx = context.WithValue(ctx, keyRole, role)

	return ctx
}

func SetUserIDOnContext(ctx context.Context, UserID int64) context.Context {
	ctx = context.WithValue(ctx, keyUserID, UserID)

	return ctx
}

func GetEmailFromContext(ctx context.Context) (string, error) {
	email, ok := ctx.Value(keyEmail).(string)
	if !ok {
		return "", error_messages.ErrorUnauthorized{Message: "tidak ada email"}
	}

	return email, nil
}

func GetRoleFromContext(ctx context.Context) (string, error) {
	role, ok := ctx.Value(keyRole).(string)
	if !ok {
		return "", error_messages.ErrorUnauthorized{Message: "tidak ada role"}
	}

	return role, nil
}

func GetUserIDFromContext(ctx context.Context) (int64, error) {
	userID, ok := ctx.Value(keyUserID).(int64)
	if !ok {
		return 0, error_messages.ErrorUnauthorized{Message: "tidak ada user ID"}
	}

	return userID, nil
}