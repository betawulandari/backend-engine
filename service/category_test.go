package service_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/constanta"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/context_value"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/repository/mocks"
	"gitlab.com/kabar-informatika-engine/backend-engine/service"
)

func TestCategoryCreate(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		email := "beta@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)
		ctx = context_value.SetRoleOnContext(ctx, constanta.SuperAdmin)

		categoryFromDelivery := entity.Category{
			Name: "ai",
		}
		categoryParam := entity.Category{
			Name:      "ai",
			CreatedBy: email,
		}
		categoryResponse := entity.Category{
			ID:        1,
			Name:      "ai",
			CreatedBy: email,
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Create", ctx, categoryParam).Return(categoryResponse, nil).Once()

		response, err := categoryService.Create(ctx, categoryFromDelivery)
		assert.Nil(t, err)
		assert.Equal(t, categoryResponse, response)
	})

	t.Run("error create", func(t *testing.T) {
		email := "beta@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)
		ctx = context_value.SetRoleOnContext(ctx, constanta.SuperAdmin)

		categoryFromDelivery := entity.Category{
			Name: "ai",
		}
		categoryParam := entity.Category{
			Name:      "ai",
			CreatedBy: email,
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Create", ctx, categoryParam).Return(entity.Category{}, errors.New("error create category")).Once()

		response, err := categoryService.Create(ctx, categoryFromDelivery)
		assert.Error(t, err)
		assert.Equal(t, entity.Category{}, response)
	})

	t.Run("error unmatch role", func(t *testing.T) {
		email := "beta@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)
		ctx = context_value.SetRoleOnContext(ctx, "viewer")

		categoryFromDelivery := entity.Category{
			Name: "ai",
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)

		response, err := categoryService.Create(ctx, categoryFromDelivery)
		assert.Error(t, err)
		assert.Equal(t, error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}, err)
		assert.Equal(t, entity.Category{}, response)
	})

	t.Run("empty role", func(t *testing.T) {
		email := "beta@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)

		categoryFromDelivery := entity.Category{
			Name: "ai",
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)

		response, err := categoryService.Create(ctx, categoryFromDelivery)
		assert.Error(t, err)
		assert.Equal(t, entity.Category{}, response)
	})

	t.Run("empty email", func(t *testing.T) {
		categoryFromDelivery := entity.Category{
			Name: "ai",
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)

		response, err := categoryService.Create(context.Background(), categoryFromDelivery)
		assert.Error(t, err)
		assert.Equal(t, entity.Category{}, response)
	})
}

func TestCategoryDelete(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		email := "betawulandari21@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)
		ctx = context_value.SetRoleOnContext(ctx, constanta.SuperAdmin)

		var IDFromDelivery int64 = 1

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Delete", ctx, IDFromDelivery).Return(nil).Once()

		err := categoryService.Delete(ctx, IDFromDelivery)
		assert.Nil(t, err)
	})

	t.Run("error", func(t *testing.T) {
		email := "betawulandari21@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)
		ctx = context_value.SetRoleOnContext(ctx, constanta.SuperAdmin)

		var IDFromDelivery int64 = 1

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Delete", ctx, IDFromDelivery).Return(errors.New("error delete category")).Once()

		err := categoryService.Delete(ctx, IDFromDelivery)
		assert.Error(t, err)
		assert.Equal(t, errors.New("error delete category"), err)
	})

	t.Run("unmatch role", func(t *testing.T) {
		email := "betawulandari21@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)
		ctx = context_value.SetRoleOnContext(ctx, "viewer")

		var IDFromDelivery int64 = 1

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)

		err := categoryService.Delete(ctx, IDFromDelivery)
		categoryRepoMock.AssertExpectations(t)
		assert.Error(t, err)
		assert.Equal(t, error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}, err)
	})

	t.Run("empty role", func(t *testing.T) {
		email := "betawulandari21@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)

		var IDFromDelivery int64 = 1

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Delete", ctx, IDFromDelivery).Return(errors.New("error delete category")).Once()

		err := categoryService.Delete(ctx, IDFromDelivery)
		assert.Error(t, err)
		assert.Equal(t, error_messages.ErrorUnauthorized{Message: "tidak ada role"}, err)
	})

	t.Run("empty email", func(t *testing.T) {
		var IDFromDelivery int64 = 1

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Delete", context.Background(), IDFromDelivery).Return(errors.New("error delete category")).Once()

		err := categoryService.Delete(context.Background(), IDFromDelivery)
		assert.Error(t, err)
		assert.Equal(t, error_messages.ErrorUnauthorized{Message: "tidak ada email"}, err)
	})
}

func TestCategoryUpdate(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		email := "betawulandari21@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)
		ctx = context_value.SetRoleOnContext(ctx, constanta.SuperAdmin)

		var IDFromDelivery int64 = 1
		category := entity.Category{
			ID: 1,
			Name: "ai",
			Status: 0,
			UpdatedBy: email,
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Update", ctx, IDFromDelivery, category).Return(nil).Once()

		err := categoryService.Update(ctx, IDFromDelivery, category)
		assert.Nil(t, err)
		assert.Equal(t, nil, err)
	})

	t.Run("error", func(t *testing.T) {
		email := "betawulandari21@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)
		ctx = context_value.SetRoleOnContext(ctx, constanta.SuperAdmin)

		var IDFromDelivery int64 = 1
		category := entity.Category{
			ID:     1,
			Name:   "tech",
			Status: 0,
			UpdatedBy: email,
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Update", ctx, IDFromDelivery, category).Return(errors.New("error update category")).Once()

		err := categoryService.Update(ctx, IDFromDelivery, category)
		assert.Error(t, err)
		assert.Equal(t, errors.New("error update category"), err)
	})

	t.Run("unmatch role", func(t *testing.T) {
		email := "betawulandari21@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)
		ctx = context_value.SetRoleOnContext(ctx, "viewer")

		var IDFromDelivery int64 = 1
		category := entity.Category{
			ID:     1,
			Name:   "ai",
			Status: 0,
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Update", ctx, IDFromDelivery, category).Return(errors.New("error update category")).Once()

		err := categoryService.Update(ctx, IDFromDelivery, category)
		assert.Error(t, err)
		assert.Equal(t, error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}, err)
	})

	t.Run("empty role", func(t *testing.T) {
		email := "betawulandari21@gmail.com"

		ctx := context.Background()
		ctx = context_value.SetEmailOnContext(ctx, email)

		var IDFromDelivery int64 = 1
		category := entity.Category{
			ID:     1,
			Name:   "ai",
			Status: 0,
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Update", ctx, IDFromDelivery, category).Return(errors.New("error update category")).Once()

		err := categoryService.Update(ctx, IDFromDelivery, category)
		assert.Error(t, err)
		assert.Equal(t, error_messages.ErrorUnauthorized{Message: "tidak ada role"}, err)
	})

	t.Run("empty email", func(t *testing.T) {
		var IDFromDelivery int64 = 1
		category := entity.Category{
			ID:     1,
			Name:   "ai",
			Status: 0,
		}

		categoryRepoMock := new(mocks.CategoryRepository)
		categoryService := service.NewCategoryService(categoryRepoMock)
		categoryRepoMock.On("Update", context.Background(), IDFromDelivery, category).Return(errors.New("error update category")).Once()

		err := categoryService.Update(context.Background(), IDFromDelivery, category)
		assert.Error(t, err)
		assert.Equal(t, error_messages.ErrorUnauthorized{Message: "tidak ada email"}, err)
	})
}
