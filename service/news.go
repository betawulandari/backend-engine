package service

import (
	"context"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/constanta"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/context_value"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/repository"
)

type newsService struct {
	newsRepo repository.NewsRepository
}

func (n newsService) Create(ctx context.Context, news entity.News) (entity.News, error) {
	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return entity.News{}, err
	}

	if role != constanta.SuperAdmin && role != constanta.Admin {
		return entity.News{}, error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}
	}

	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return entity.News{}, err
	}

	userID, err := context_value.GetUserIDFromContext(ctx)
	if err != nil {
		return entity.News{}, error_messages.ErrorUnauthorized{Message: err.Error()}
	}
	news.UserID = userID

	news.CreatedBy = email
	news, err = n.newsRepo.Create(ctx, news)
	if err != nil {
		return entity.News{}, err
	}

	return news, nil
}

func (n newsService) Update(ctx context.Context, ID int64, news entity.News) error {
	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return err
	}

	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return err
	}

	if role != constanta.SuperAdmin && role != constanta.Admin {
		return error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}
	}

	news.UpdatedBy = email
	err = n.newsRepo.Update(ctx, ID, news)
	if err != nil {
		return err
	}

	return nil
}

func (n newsService) Publish(ctx context.Context, ID int64) error {
	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return err
	}

	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return err
	}

	if role != constanta.SuperAdmin && role != constanta.Admin {
		return error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}
	}

	err = n.newsRepo.Publish(ctx, ID, email)
	if err != nil {
		return err
	}

	return nil
}

func (n newsService) UnPublish(ctx context.Context, ID int64) error {
	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return err
	}

	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return err
	}

	if role != constanta.SuperAdmin && role != constanta.Admin {
		return error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}
	}

	err = n.newsRepo.UnPublish(ctx, ID, email)
	if err != nil {
		return err
	}

	return nil
}

func (n newsService) Read(ctx context.Context, filter entity.NewsFilter) (entity.NewsResponse, error) {
	news, err := n.newsRepo.Read(ctx, filter)
	if err != nil {
		return entity.NewsResponse{}, err
	}

	total, err := n.newsRepo.Count(ctx, filter)
	if err != nil {
		return entity.NewsResponse{}, err
	}

	response := entity.NewsResponse{
		News:  news,
		Total: total,
	}

	return response, nil
}

func (n newsService) ReadById(ctx context.Context, ID int64) (entity.News, error) {
	var news entity.News

	news.ID = ID
	news, err := n.newsRepo.ReadById(ctx, ID)
	if err != nil {
		return entity.News{}, err
	}

	return news, nil
}

func NewNewsService(newsRepo repository.NewsRepository) NewsService {
	return newsService{
		newsRepo: newsRepo,
	}
}
