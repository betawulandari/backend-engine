package service

import (
	"context"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/constanta"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/context_value"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/repository"
)

type categoryService struct {
	categoryRepo repository.CategoryRepository
}

func (c categoryService) Create(ctx context.Context, category entity.Category) (entity.Category, error) {
	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return entity.Category{}, err
	}
	category.CreatedBy = email
	
	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return entity.Category{}, err
	}

	if role != constanta.SuperAdmin && role != constanta.Admin {
		return entity.Category{}, error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}
	}

	category, err = c.categoryRepo.Create(ctx, category)
	if err != nil {
		return entity.Category{}, err
	}

	return category, nil
}

func (c categoryService) Read(ctx context.Context, filter entity.CategoryFilter) (entity.CategoryResponse, error) {
	categories, err := c.categoryRepo.Read(ctx, filter)
	if err != nil {
		return entity.CategoryResponse{}, err
	}

	total, err := c.categoryRepo.Count(ctx, filter)
	if err != nil {
		return entity.CategoryResponse{}, err
	}

	response := entity.CategoryResponse{
		Categories: categories,
		Total:      total,
	}

	return response, nil
}

func (c categoryService) Update(ctx context.Context, ID int64, category entity.Category) error {
	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return err
	}

	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return err
	}

	// false & true = false
	// src: https://stackoverflow.com/questions/62470008/error-when-building-getting-suspect-or
	if role != constanta.SuperAdmin && role != constanta.Admin {
		return error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}
	}

	category.UpdatedBy = email
	err = c.categoryRepo.Update(ctx, ID, category)
	if err != nil {
		return err
	}

	return nil
}

func (c categoryService) Delete(ctx context.Context, ID int64) error {
	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return err
	}

	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return err
	}

	if role != constanta.SuperAdmin && role != constanta.Admin {
		return error_messages.ErrorUnauthorized{Message: "tidak boleh mengakses"}
	}

	var category entity.Category
	category.UpdatedBy = email

	err = c.categoryRepo.Delete(ctx, ID)
	if err != nil {
		return err
	}

	return nil
}

func NewCategoryService(categoryRepo repository.CategoryRepository) CategoryService {
	return categoryService{
		categoryRepo: categoryRepo,
	}
}
