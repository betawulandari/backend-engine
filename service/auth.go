package service

import (
	"context"
	"time"

	"github.com/golang-jwt/jwt"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/jwt_hash"
	"gitlab.com/kabar-informatika-engine/backend-engine/repository"
)

type authService struct {
	authRepo repository.AuthRepository
	jwtRepo  jwt_hash.JwtHash
}

func (a authService) Login(ctx context.Context, email string, password string) (string, error) {
	user, err := a.authRepo.Login(ctx, email)
	if err != nil {
		return "", err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return "", error_messages.ErrorUnauthorized{Message: err.Error()}
	}

	claim := jwt_hash.Claim{
		Email:          user.Email,
		Role:           user.Role,
		UserID:         user.ID,
		StandardClaims: jwt.StandardClaims{ExpiresAt: time.Now().Add(time.Duration(1) * time.Hour).Unix()}, // masa berlaku hanya 1 jam
	}

	token, err := a.jwtRepo.Encode(claim)
	if err != nil {
		return "", err
	}

	return token, nil
}

func NewAuthService(authRepo repository.AuthRepository, jwtRepo jwt_hash.JwtHash) AuthService {
	return authService{
		authRepo: authRepo,
		jwtRepo:  jwtRepo,
	}
}
