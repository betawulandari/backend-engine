package service

import (
	"context"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
)

type UserService interface {
	Create(ctx context.Context, user entity.User) (entity.User, error)
	Read(ctx context.Context, filter entity.UserFilter) (entity.UserResponse, error)
	ReadById(ctx context.Context, ID int64) (entity.User, error)
	Update(ctx context.Context, ID int64, user entity.User) error
	Delete(ctx context.Context, ID int64) error
}

type AuthService interface {
	Login(ctx context.Context, email string, password string) (string, error)
}

type CategoryService interface {
	Create(ctx context.Context, category entity.Category) (entity.Category, error)
	Read(ctx context.Context, filter entity.CategoryFilter) (entity.CategoryResponse, error)
	Update(ctx context.Context, ID int64, category entity.Category) error
	Delete(ctx context.Context, ID int64) error
}

type NewsService interface {
	Create(ctx context.Context, news entity.News) (entity.News, error)
	Update(ctx context.Context, ID int64, news entity.News) error
	Publish(ctx context.Context, ID int64) error
	UnPublish(ctx context.Context, ID int64) error
	Read(ctx context.Context, filter entity.NewsFilter) (entity.NewsResponse, error)
	ReadById(ctx context.Context, ID int64) (entity.News, error)
}