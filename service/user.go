package service

import (
	"context"
	"fmt"

	"gitlab.com/kabar-informatika-engine/backend-engine/entity"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/constanta"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/context_value"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/error_messages"
	"gitlab.com/kabar-informatika-engine/backend-engine/repository"
	"golang.org/x/crypto/bcrypt"
)

type userService struct {
	userRepo  repository.UserRepository
	emailRepo repository.EmailRepository
}

func (u userService) Create(ctx context.Context, user entity.User) (entity.User, error) {
	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return entity.User{}, error_messages.ErrorUnauthorized{Message: err.Error()}
	}
	if role != constanta.SuperAdmin {
		return entity.User{}, error_messages.ErrorUnauthorized{Message: "role not super admin"}
	}

	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return entity.User{}, error_messages.ErrorUnauthorized{Message: err.Error()}
	}
	user.CreatedBy = email

	// jika email sudah ada tidak diperbolehkan
	filter := entity.UserFilter{Email: user.Email}
	users, err := u.userRepo.Read(ctx, filter)
	if err != nil {
		return entity.User{}, err
	}
	if len(users) > 0 {
		return entity.User{}, error_messages.ErrorDuplicate{Message: "email sudah ada"}
	}

	plainTextPassword := user.Password
	bytePasword, err := bcrypt.GenerateFromPassword([]byte(plainTextPassword), bcrypt.DefaultCost)
	if err != nil {
		return entity.User{}, err
	}
	user.Password = string(bytePasword)

	user, err = u.userRepo.Create(ctx, user)
	if err != nil {
		return entity.User{}, err
	}
	user.Password = ""

	err = u.emailRepo.Send(user.Email, plainTextPassword)
	if err != nil {
		fmt.Println("error: ", err)
	}

	return user, nil
}

func (u userService) Read(ctx context.Context, filter entity.UserFilter) (entity.UserResponse, error) {
	var user entity.User
	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return entity.UserResponse{}, err
	}

	user.Role = role
	users, err := u.userRepo.Read(ctx, filter)
	if err != nil {
		return entity.UserResponse{}, err
	}

	total, err := u.userRepo.Count(ctx, filter)
	if err != nil {
		return entity.UserResponse{}, err
	}

	response := entity.UserResponse{
		Users: users,
		Total: total,
	}

	return response, nil
}

func (u userService) Delete(ctx context.Context, ID int64) error {
	// 1. select by id
	// 2. dapat datanya
	// 3. jika role = super admin maka return error(gak boleh dihapus)
	// 4. selain super admin boleh dihapus
	var user entity.User
	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return err
	}

	user.UpdatedBy = email
	user, err = u.userRepo.ReadById(ctx, ID)
	if err != nil {
		return err
	}

	if user.Role == constanta.SuperAdmin {
		return error_messages.ErrorNotAllowed{Message: "super admin tidak boleh dihapus"}
	}

	err = u.userRepo.Delete(ctx, ID)
	if err != nil {
		return err
	}

	return nil
}

func (u userService) Update(ctx context.Context, ID int64, user entity.User) error {
	email, err := context_value.GetEmailFromContext(ctx)
	if err != nil {
		return err
	}

	user.UpdatedBy = email
	err = u.userRepo.Update(ctx, ID, user)
	if err != nil {
		return err
	}

	return nil
}

func (u userService) ReadById(ctx context.Context, ID int64) (entity.User, error) {
	var user entity.User
	role, err := context_value.GetRoleFromContext(ctx)
	if err != nil {
		return entity.User{}, err
	}

	user.Role = role
	user, err = u.userRepo.ReadById(ctx, ID)
	if err != nil {
		return entity.User{}, err
	}

	return user, nil
}

func NewUserService(userRepo repository.UserRepository, emailRepo repository.EmailRepository) UserService {
	return userService{
		userRepo:  userRepo,
		emailRepo: emailRepo,
	}
}
