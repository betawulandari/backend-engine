package main

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql" // auto load pertama kali
	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
	echoSwagger "github.com/swaggo/echo-swagger"

	"gitlab.com/kabar-informatika-engine/backend-engine/delivery"
	_ "gitlab.com/kabar-informatika-engine/backend-engine/docs"
	"gitlab.com/kabar-informatika-engine/backend-engine/middleware"
	"gitlab.com/kabar-informatika-engine/backend-engine/pkg/jwt_hash"
	"gitlab.com/kabar-informatika-engine/backend-engine/repository"
	"gitlab.com/kabar-informatika-engine/backend-engine/service"
)

// @title Swagger API Kabar Informatika
// @version 2.0
// @description This is a sample server Petstore server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
func main() {
	viper.AutomaticEnv()
	viper.SetConfigType("env")
	viper.SetConfigName(".env")
	viper.AddConfigPath(".") // . brarti selevel dgn main
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal("failed running because file .env")
	}

	dsn := viper.GetString("mysql_dsn")
	secretKey := viper.GetString("secret_key")
	emailName := viper.GetString("email_name")
	emailPassword := viper.GetString("email_password")

	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal("can't connect database")
	}

	userRepo := repository.NewUserRepository(db)
	authRepo := repository.NewAuthRepository(db)
	categoryRepo := repository.NewCategoryRepository(db)
	newsRepo := repository.NewNewsRepository(db)
	emailRepo := repository.NewEmailRepository("smtp.gmail.com", 587, "Kabar Informatika", emailName, emailPassword)

	jwtHash := jwt_hash.NewJwtHash([]byte(secretKey))

	userService := service.NewUserService(userRepo, emailRepo)
	authService := service.NewAuthService(authRepo, jwtHash)
	categoryService := service.NewCategoryService(categoryRepo)
	newsService := service.NewNewsService(newsRepo)

	e := echo.New()
	handlerMiddleware := middleware.HandlerMiddleware{JwtHash: jwtHash}
	e.Use(handlerMiddleware.Auth)

	delivery.RegisterUserRoute(userService, e)
	delivery.RegisterAuthRoute(authService, e)
	delivery.RegisterCategoryRoute(categoryService, e)
	delivery.RegisterNewsRoute(newsService, e)

	e.GET("/swagger/*", echoSwagger.WrapHandler)

	e.Logger.Fatal(e.Start(":8080"))
}
