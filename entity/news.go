package entity

import "time"

type News struct {
	ID          int64      `json:"id" swaggerignore:"true"`
	Title       string     `json:"title"`
	Content     string     `json:"content"`
	Thumbnail   string     `json:"thumbnail"`
	Image       string     `json:"image"`
	UserID      int64      `json:"user_id" swaggerignore:"true"`
	CategoryIDs []int      `json:"category_ids"`
	Categories  []Category `json:"categories" swaggerignore:"true"`
	Status      int        `json:"status" swaggerignore:"true"`
	CreatedAt   time.Time  `json:"-"`
	UpdatedAt   time.Time  `json:"-"`
	CreatedBy   string     `json:"-"`
	UpdatedBy   string     `json:"-"`
}

type NewsFilter struct {
	Limit        uint64
	Page         int
	Title        string
	Order        string
	Status       []int
	UserID       int
	CategoryName string
	CategoryID   int
}

type NewsResponse struct {
	News  []News `json:"newses"`
	Total int    `json:"total"`
}
