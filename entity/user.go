package entity

import "time"

type User struct {
	ID        int64     `json:"id" swaggerignore:"true"`
	Name      string    `json:"name"`
	Email     string    `json:"email"`
	Role      string    `json:"role" swaggerignore:"true"`
	Password  string    `json:"password"`
	Status    int       `json:"status" swaggerignore:"true"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	CreatedBy string    `json:"-"`
	UpdatedBy string    `json:"-"`
}

type UserFilter struct {
	Limit  uint64 // sama seperti int64 tp bedanya uint64 positif saja
	Page   int
	Name   string
	Email  string
	Order  string
	Role   string
	Status []int
}

type UserResponse struct {
	Users []User `json:"users"`
	Total int    `json:"total"`
}
