package entity

import "time"

type Category struct {
	ID        int64     `json:"id" swaggerignore:"true"` 
	Name      string    `json:"name"`
	Status    int       `json:"status" swaggerignore:"true"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	CreatedBy string    `json:"-"`
	UpdatedBy string    `json:"-"`
}

type CategoryFilter struct {
	Limit  uint64
	Page   int
	Name   string
	Order  string
	Status []int
}

type CategoryResponse struct {
	Categories []Category `json:"categories"`
	Total      int        `json:"total"`
}
